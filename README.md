# Sprite #

v1.0.6

This module handles sprite animation

## Exemple ##

```js
var canvas = document.getElementById("myCanvas");

var sprite = new sprite(canvas, "../img/JSON_ARRAY/chevalier.png", "../img/JSON_ARRAY/chevalier.json",40, 1, sprite.startAnimationLoop);

sprite.addOnEndEvent(function(){
	sprite.backLoop();
});
```