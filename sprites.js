define([
	'require',
	'madjoh_modules/styling/styling'
],
function(require, Styling){
	var Sprites = {
		init : function(SpriteSettings){
			if(!document.sprites) document.sprites = {};
			Sprites.sources = SpriteSettings;

			var promises = [];
			for(var spriteKey in SpriteSettings) promises.push(Sprites.getJSON(spriteKey));
 
			return Promise.all(promises).catch(function(error){
				if(error.printStackTrace) error.printStackTrace();
				else console.log(error);
			});
		},

		getJSON : function(spriteKey){
			if(!Sprites.sources[spriteKey]) return reject('SPRITE_SOURCE_NOT_FOUND : ' + spriteKey);

				 if(document.sprites[spriteKey] && document.sprites[spriteKey].value) 	return Promise.resolve(document.sprites[spriteKey].value);
			else if(document.sprites[spriteKey] && document.sprites[spriteKey].promise) return document.sprites[spriteKey].promise;

			document.sprites[spriteKey] = {};
			document.sprites[spriteKey].promise = new Promise(function(resolve, reject){
				var url = 'app/images/' + Styling.getSizeCategory() + '/' + Sprites.sources[spriteKey].json;
				var xhr = new XMLHttpRequest();
					xhr.open('GET', url, true);
					xhr.onreadystatechange = function(){
						if(xhr.readyState === 4){
							document.sprites[spriteKey].value = JSON.parse(xhr.responseText);
							return resolve(document.sprites[spriteKey].value);
						}
					};

				try{xhr.send(null);}
				catch(e){return reject(e);}
			});
			return document.sprites[spriteKey].promise;
		}
	};

	return Sprites;
});