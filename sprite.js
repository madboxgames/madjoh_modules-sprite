define([
	'require',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/page_manager/tools',
	'madjoh_modules/sprite/sprites'
],
function (require, CustomEvents, Tools, Sprites){
	var tools = new Tools();

	// BUILD SPRITE
		function Sprite(canvas, spriteKey, ticksPerFrame, scale){
			// ATTRIBUTS 
				this.canvas 		= canvas;
				this.image 			= tools.getImage(Sprites.sources[spriteKey].img);

				this.scale 			= scale 		|| 1;
				this.ticksPerFrame 	= ticksPerFrame || 25;
				this.loops 			= 0;
				this.sequenceIndex 	= 0;
				this.frameIndex 	= 0;
				this.playState 		= false;

			// LOAD JSON DESCRIPTION
				var sprite = this;
				Sprites.getJSON(spriteKey).then(function(description){
					sprite.description = description;

					var minX = 1000;
					var minY = 1000;

					for(var i = 0; i < sprite.description.frames.length; i++){
						var dimension2 = sprite.description.frames[i].spriteSourceSize;
						if(dimension2.x < minX) minX = dimension2.x;
						if(dimension2.y < minY) minY = dimension2.y;
					}

					for(var i = 0; i < sprite.description.frames.length; i++){
						sprite.description.frames[i].spriteSourceSize.x -= minX;
						sprite.description.frames[i].spriteSourceSize.y -= minY;
					}

					sprite.setScale(sprite.scale);

					CustomEvents.fireCustomEvent(sprite, 'DESCRIPTION_LOADED');
				});
		}

	// ANIMATE SPRITE
		Sprite.prototype.render = function(){
			var dimension1 = this.description.frames[this.frameIndex].frame;
			var dimension2 = this.description.frames[this.frameIndex].spriteSourceSize;

			this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
			this.canvas.getContext('2d').drawImage(
				this.image,
							 dimension1.x, 				 dimension1.y, 				 dimension1.w, 				 dimension1.h,
				this.scale * dimension2.x, 	this.scale * dimension2.y, 	this.scale * dimension1.w, 	this.scale * dimension1.h
			);
		};
		Sprite.prototype.onEnd = function(){
			this.playState = false;
			if(this.animationTimer) clearTimeout(this.animationTimer);

			if(this.loops === 0){
				CustomEvents.fireCustomEvent(this, 'ANIMATION_END');
			}else{
				this.loops -= (this.loops > 0) ? 1 : 0;
				CustomEvents.fireCustomEvent(this, 'ANIMATION_ITERATE');
				this.backLoop();
			}
		};
		Sprite.prototype.backLoop = function(){
			var sprite = this;
			this.animationTimer = setTimeout(function(){sprite.startAnimationLoop();}, this.ticksPerFrame);
		};

	// PUBLIC API
		Sprite.prototype.startAnimationLoop = function(index, loops){
			var sprite = this;
			if(!this.description){
				var eventId = CustomEvents.addCustomEventListener(this, 'DESCRIPTION_LOADED', function(){
					CustomEvents.removeCustomEventListener(sprite, 'DESCRIPTION_LOADED', eventId);
					sprite.startAnimationLoop(index, loops);
				});
				return;
			}

			// START NEW
			this.sequenceIndex 	= (typeof(index) === 'undefined') ? this.sequenceIndex 	|| 0 : index;
			this.loops 			= (typeof(loops) === 'undefined') ? this.loops 			|| 0 : loops;

			var sequence = this.description.sequences[this.sequenceIndex];
			this.animate(sequence.start, sequence.end);
		};
		Sprite.prototype.animate = function(start, end){
			var sprite = this;
			if(!this.description){
				var eventId = CustomEvents.addCustomEventListener(this, 'DESCRIPTION_LOADED', function(){
					CustomEvents.removeCustomEventListener(sprite, 'DESCRIPTION_LOADED', eventId);
					sprite.animate(start, end);
				});
				return;
			}
			// STOP ANY PREVIOUS
			if(this.animationTimer) clearTimeout(this.animationTimer);

			this.playState = true;
			this.frameIndex = start;
			this.render();

				 if(start < end) 	this.animationTimer = setTimeout(function(){sprite.animate(start + 1, end);}, this.ticksPerFrame);
			else if(start > end) 	this.animationTimer = setTimeout(function(){sprite.animate(start - 1, end);}, this.ticksPerFrame);
			else 					this.onEnd();
		};
		Sprite.prototype.stop = function(){
			this.playState = false;
			if(this.animationTimer) clearTimeout(this.animationTimer);
		};

		Sprite.prototype.setScale = function(scale){
			this.scale = scale;

			var maxW = 0;
			var maxH = 0;

			for(var i = 0; i < this.description.frames.length; i++){
				var dimension1 = this.description.frames[i].frame;
				var dimension2 = this.description.frames[i].spriteSourceSize;
				if(dimension1.w + dimension2.x > maxW) maxW = dimension1.w + dimension2.x;
				if(dimension1.h + dimension2.y > maxH) maxH = dimension1.h + dimension2.y;  
			}

			this.canvas.width 	= 3 * this.scale * maxW;
			this.canvas.height 	= 3 * this.scale * maxH;
			this.canvas.style.width 	= this.scale * maxW + 'px';
			this.canvas.style.height 	= this.scale * maxH + 'px';
			this.canvas.getContext('2d').scale(3, 3);

			this.render();
		};
		Sprite.prototype.expandScale = function(coeff){
			this.setScale(this.scale * coeff);
		};

	return Sprite;
});